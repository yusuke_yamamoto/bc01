pragma solidity ^0.4.0;
contract SingleNumRegister {
    string storedData;
    uint status;
    string created_at;
    string updated_at;
    string sender;
    string receiver;

    function set(string x) public{
        storedData = x;
        status = 0;
    }
    function set_receiver(string x) public{
        receiver = x;
    }
    function set_sender(string x) public{
        sender = x;
    }
    function set_created_at(string x) public{
        created_at = x;
    }
    function set_updated_at(string x) public{
        updated_at = x;
    }
    function get() public constant returns (string retVal){
        return storedData;
    }
    function get_status() public constant returns (uint retVal){
        return status;
    }
    function get_created_at() public constant returns (string retVal){
        return created_at;
    }
    function get_updated_at() public constant returns (string retVal){
        return updated_at;
    }
    function get_sender() public constant returns (string retVal){
        return sender;
    }
    function get_receiver() public constant returns (string retVal){
        return receiver;
    }
    function approve() public{
        status = 1;
    }
    function done() public{
        status = 2;
    }
}
