class RenameTypeColumnToCompanies < ActiveRecord::Migration[5.2]
  def change
    rename_column :companies, :type, :format
  end
end
