class CreateContracts < ActiveRecord::Migration[5.2]
  def change
    create_table :contracts do |t|
      t.text :storedData
      t.integer :status
      t.text :sender
      t.text :receiver

      t.timestamps
    end
  end
end
