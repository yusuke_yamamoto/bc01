class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.integer :from
      t.integer :to
      t.integer :event_flag
      t.date :logged_at

      t.timestamps
    end
  end
end
