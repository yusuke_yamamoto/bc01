class AddDetailsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :birth, :date
    add_column :users, :occupation, :string
    add_column :users, :age, :integer
    add_column :users, :profile, :string
  end
end
