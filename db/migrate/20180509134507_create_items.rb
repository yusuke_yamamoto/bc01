class CreateItems < ActiveRecord::Migration[5.2]
  def change
    create_table :items do |t|
      t.string :title
      t.string :description
      t.integer :price
      t.string :author
      t.string :company
      t.integer :type_id
      t.integer :category_id
      t.date :sold_from

      t.timestamps
    end
  end
end
