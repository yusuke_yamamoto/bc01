class ContactMailer < ActionMailer::Base
  default from: "yusuke.yamamoto.2014@gmail.com"
  default to: "yusuke.yamamoto.2014@gmail.com"

  def received_email(contact)
    @contact = contact
    mail(:subject => 'お問い合わせを承りました')
  end

end
