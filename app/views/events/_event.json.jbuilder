json.extract! event, :id, :from, :to, :event_flag, :logged_at, :created_at, :updated_at
json.url event_url(event, format: :json)
