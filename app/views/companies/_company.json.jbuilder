json.extract! company, :id, :name, :type, :created_at, :updated_at
json.url company_url(company, format: :json)
