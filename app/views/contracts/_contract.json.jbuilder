json.extract! contract, :id, :storedData, :status, :sender, :receiver, :created_at, :updated_at
json.url contract_url(contract, format: :json)
