module ApplicationHelper

  # ページごとの完全なタイトルを返します。
  def full_title(page_title = '')
    base_title = "Revie"
    if page_title.empty?
      "レビュー・感想を" + base_title + "で共有！本でも映画でもOK"
    else
      page_title + " | " + base_title
    end
  end
end
