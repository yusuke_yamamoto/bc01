require 'ethereum'

class ReviewsController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user,   only: :destroy
  # GET /reviews
  # GET /reviews.json
  def index
  end

  # GET /reviews/1
  # GET /reviews/1.json
  def show
  end

  # GET /reviews/new
  def new
  end

  # GET /reviews/1/edit
  def edit
    contract = Ethereum::Contract.create(file: "test.sol", address: $address, client: $client)
    contract.transact_and_wait.set(review_params[:point].to_i)
    redirect_to request.referrer || root_url    
  end

  # POST /reviews
  # POST /reviews.json
  def create
    contract = Ethereum::Contract.create(file: "test.sol", address: $address, client: $client)
    contract.transact_and_wait.set(review_params[:point].to_i)
    redirect_to request.referrer || root_url
  end

  # PATCH/PUT /reviews/1
  # PATCH/PUT /reviews/1.json
  def update
    @review = Review.find(params[:id])
    if @review.update_attributes(review_params)
      flash[:success] = "レビューを更新しました"
      redirect_to @review
    else
      render 'edit'
    end
  end
  # DELETE /reviews/1
  # DELETE /reviews/1.json
  def destroy
    @review.destroy
    flash[:success] = "レビューを削除しました"
    redirect_to request.referrer || root_url
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def review_params
      params.require(:review).permit(:content,:user_id, :item_id, :point, :picture, :title)
    end

    def correct_user
      @review = current_user.reviews.find_by(id: params[:id])
      redirect_to root_url if @review.nil?
    end
end
