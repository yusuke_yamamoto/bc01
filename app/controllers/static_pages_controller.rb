require 'ethereum'

class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @user = User.find(session[:user_id])
    end
    if  defined?($address)
      contract = Ethereum::Contract.create(file: "test.sol", address: $address, client: $client)
      @get = contract.call.get();
      @get_sender = contract.call.get_sender();
      @get_receiver = contract.call.get_receiver();
      @get_status = contract.call.get_status();
      @get_created_at = contract.call.get_created_at();
      @get_updated_at = contract.call.get_updated_at();
    else
      @get = "none"
    end
  end

  def help
  end

  def aboutus
  end

end
