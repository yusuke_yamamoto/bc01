var baseurl = "/typeahead?term=%QUERY"

var qst1 = { title: 'title' } //'name'はカラム名

var url1 = baseurl + "&" + jQuery.param(qst1)
// jQuery.param(params)はオブジェクトqst1をシリアライズ
// url1 =  "/typeahead?term=%QUERY&item=name"

var itemtiles = new Bloodhound({

  datumTokenizer: function(d) {return Bloodhound.tokenizers.whitespace([d.title]); }, //d.nameのnameはカラム名
  queryTokenizer: Bloodhound.tokenizers.whitespace,

  remote: {
        url: url1, wildcard: '%QUERY'
        }
});

// 上記で定義したusernamesの初期化
itemtiles.initialize();

  jQuery( document ).ready(function( $ ) {
      $('#item_title.typeahead').typeahead({ // #user_nameは後ほどViewファイルのフォーム部分に付与するid属性名
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'title',       // 'name'はカラム名
      displayKey: 'title', // 'name'はカラム名
      source: itemtiles.ttAdapter()
　　}).on("typeahead:selected typeahead:autocomplete", function(e, datum) {
    return $('#item_title.typeahead').val(datum.title);
    // #user_nameは後ほどViewファイルのフォーム部分に付与するid属性名
    // datum.nameのnameはカラム名
  });
});
