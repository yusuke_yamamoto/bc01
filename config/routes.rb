Rails.application.routes.draw do
  resources :events
  resources :corps
  resources :companies
  resources :contracts
  get 'contact/index'
  resources :comments
  resources :types
  resources :categories
  root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/aboutus',    to: 'static_pages#aboutus'
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get 'contact',  to: 'contact#index'
  post 'contact/confirm',  to: 'contact#confirm'
  post 'contact/thanks',  to: 'contact#thanks'
  get   '/typeahead' => 'items#typeahead_action'
  get   '/create_contract',   to: 'contracts#make_new'
  post   '/update_contract',   to: 'contracts#change_data'
  post   '/get_contract',   to: 'contracts#get_contract'
  post   '/create_event',   to: 'events#make_new'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :items
  resources :updates
  resources :reviews
  resources :relationships,       only: [:create, :destroy]

end
